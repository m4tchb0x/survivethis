'use strict';

function Player(game,map) {
	this.map = map;
	this.game = game;
	this.cursors = null;
	this.sprite = null;
	this.sprite2 = null;
	this.speed = 120;
	this.alive = false;
};

Player.prototype = {

	create: function () {

		this.sprite = this.game.add.sprite(-100, -100, 'player');
		this.sprite.anchor.setTo(0.5, 0.5);
		this.game.physics.arcade.enable(this.sprite);
		this.sprite.body.setSize(36,36,0,0)
		this.sprite.scale.x = 1.4;
		this.sprite.scale.y = 1.4;

		


		this.sprite.animations.add('walk', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 18, true);
		this.sprite.animations.add('run', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 30, true);
		this.sprite.animations.add('stand', [4], 15, true);
		

		this.sprite.body.collideWorldBounds = true;
		this.game.camera.follow(this.sprite);
		//this.sprite.body.setCircle(15);
		this.cursors = this.game.input.keyboard.createCursorKeys();
	},

	spawn: function(x, y) {
		if(this.alive)
			return;
		this.alive = true;
		//console.log(x,y);
		this.sprite.x = x;
		this.sprite.y = y;
	},

	damage: function() {

		// this.health -= 1;600

		// if (this.health <= 0)
		// {
		// 	this.alive = false;

		// 	this.shadow.kill();
		// 	this.tank.kill();
		// 	this.turret.kill();

		// 	return true;
		// }

		// return false;

	},

	update: function() {
		// this.game.physics.arcade.collide(this.sprite,this.sprite2);
		// if(this.sprite,this.map)
		// 	this.game.physics.arcade.collide(this.map.collisionLayer,this.sprite);
		if(!this.alive){
			this.speed = 0;
			return;
		}
		var velocity = new Phaser.Point(0,0);
		if (this.cursors.left.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.A))
			velocity.x  += -1;
		if (this.cursors.right.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.D))
			velocity.x  += 1;
		if (this.cursors.up.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.W))
			velocity.y  += -1;  
		if (this.cursors.down.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.S))
			velocity.y  += 1;

		//Speed
		var maxSpeedChange = 15;
		if(velocity.x !== 0 || velocity.y !== 0){
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)){
				if(this.speed < 270)
					this.speed += maxSpeedChange;//2000;//
				this.sprite.animations.play('run');
			}else{
				if(this.speed > 150)
					this.speed -= maxSpeedChange;
				else if(this.speed < 150)
					this.speed += maxSpeedChange;
				this.sprite.animations.play('walk');
			}
		}else{
			//if(this.speed > 0)
				this.speed = 0;
			this.sprite.animations.play('stand');
		}

		velocity.normalize();
		velocity.multiply(this.speed,this.speed);
		this.sprite.body.velocity = velocity;
		
		
		//running angle but easy
		//var maxAngle = 90/2;
		var newAngle = Math.atan2(velocity.y, velocity.x) * 180/Math.PI + 90;
		if(this.speed > 0)
			this.sprite.angle = newAngle;
		// if(this.speed > 0 && this.sprite.angle !== newAngle ){
		// 	if(this.sprite.angle > newAngle){
		// 		this.sprite.angle = newAngle + maxAngle;
		// 	}else if(this.sprite.angle < newAngle){
		// 		this.sprite.angle = newAngle - maxAngle;
		// 	}
		// }
		//running angle
		//this.sprite.angle = Math.atan2(velocity.y, velocity.x) * 180/Math.PI + 90;
		//mouse angle
		//this.sprite.rotation = this.game.physics.angleToPointer(this.sprite) + Math.PI/2;

	}

};