'use strict';

function Fog(game, sprite, tileLayer, radius ) {
	this.tileLayer = tileLayer; //layer that blocks vision
	this.sprite = sprite;
	this.game = game;
	this.graphics = this.game.add.graphics(0, 0);
	if (arguments.length === 3) { 
		this.radius = null;
	}else{
		this.radius = radius;
	}
};

Fog.prototype = {

	create: function () {
		//this.fog = this.game.add.sprite(this.game.stage.canvas.width/2, this.game.stage.canvas.height/2, 'fog');
		// this.fog.anchor.setTo(0.5, 0.5);
		// this.fog.scale.setTo(this.game.stage.canvas.width/this.fog.width, this.game.stage.canvas.width/this.fog.width);
		// this.fog.fixedToCamera = true;
	},
	update: function() {
		if(this.tileLayer !== null){
			//Get surrounding tiles
			var tiles = this.tileLayer.getTiles(this.game.camera.x, this.game.camera.y, this.game.camera.width, this.game.camera.height, false);
			this.graphics.clear();
			for(var index = 0; index < tiles.length; index++ ){

				if(this.radius){
					// this.graphics.lineStyle(20, 0x000000, 0.5);
					// var center = new Phaser.Point(tiles[index].worldX + tiles[index].width / 2
					// 								,tiles[index].worldY + tiles[index].height / 2);
					// var angle = this.game.physics.arcade.angleBetween(this.sprite,center) + Math.PI;
					// var smallest = new Phaser.Point(center.x + this.radius * Math.cos(angle), center.y + this.radius * Math.sin(angle+ Math.PI));
					// var largest = new Phaser.Point(center.x + this.radius * Math.cos(angle + Math.PI), center.y + this.radius * Math.sin(angle));
					// smallest.angle = this.game.physics.arcade.angleBetween(this.sprite, smallest);
					// largest.angle = this.game.physics.arcade.angleBetween(this.sprite, largest);
					this.graphics.lineStyle(10, 0x000000, 0.5);
					var center = new Phaser.Point(tiles[index].worldX + tiles[index].width / 2
					 								,tiles[index].worldY + tiles[index].height / 2)
					var angle = this.game.physics.arcade.angleBetween(this.sprite, center) + Math.PI/2;
					var smallest = new Phaser.Point(center.x + this.radius * Math.cos(angle), center.y + this.radius * Math.sin(angle));
					var largest = new Phaser.Point(center.x + this.radius * Math.cos(angle + Math.PI), center.y + this.radius * Math.sin(angle + Math.PI));
					smallest.angle = this.game.physics.arcade.angleBetween(this.sprite, smallest);
					largest.angle = this.game.physics.arcade.angleBetween(this.sprite, largest);
				}else{
					var points = [new Phaser.Point(tiles[index].worldX, tiles[index].worldY),
								  new Phaser.Point(tiles[index].right, tiles[index].worldY),
								  new Phaser.Point(tiles[index].worldX, tiles[index].bottom),
								  new Phaser.Point(tiles[index].right, tiles[index].bottom)];
					var smallest = null;
					var largest = null;
					var sprite = this.sprite;

					var found = _.where(points, function(point){
						return point.x < sprite.x;
					});

					if (found.length === 4 )
					{
						var finalFound = _.map(_.sortBy(found, 'x'), _.values);
						//console.log(finalFound);

						smallest = finalFound[2];
						smallest.x = smallest[0];
						smallest.y = smallest[1];
						smallest.angle = this.game.physics.arcade.angleBetween(this.sprite, smallest);
						largest = finalFound[3];
						largest.x = largest[0];
						largest.y = largest[1];
						largest.angle = this.game.physics.arcade.angleBetween(this.sprite, largest);
						if(!(smallest.y < this.sprite.y && largest.y > this.sprite.y ||
							smallest.y > this.sprite.y && largest.y < this.sprite.y )){
							largest = null;
							smallest = null;
						}
					}

					if(smallest == null && largest == null){
						for(var pointIndex = 0; pointIndex < points.length; pointIndex++ ){
							var point = points[pointIndex];
							point.angle = this.game.physics.arcade.angleBetween(this.sprite, point);
							
							if(smallest === null){
								smallest = point;
								largest = point;
								continue;
							}

							if(smallest.angle > point.angle){
								smallest = point;
							}else if(largest.angle < point.angle){
								largest = point;
							}
						}
					}
				}
				this.graphics.beginFill(0x000000);
				var smallBack = new Phaser.Point(smallest.x + 800 * Math.cos(smallest.angle),smallest.y + 800 * Math.sin(smallest.angle));
				var largeBack = new Phaser.Point(largest.x + 800 * Math.cos(largest.angle),largest.y + 800 * Math.sin(largest.angle));

				this.graphics.moveTo(smallest.x, smallest.y);
				this.graphics.lineTo(smallBack.x, smallBack.y);
				this.graphics.lineTo(largeBack.x, largeBack.y);
				this.graphics.lineTo(largest.x, largest.y);

				//this.graphics.lineTo(smallest.x, smallest.y);
				this.graphics.endFill();
				
			}
		}
		//this.fog.bringToTop();
	}
};