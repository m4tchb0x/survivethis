(function() {
	'use strict';

	function Game() {
		this.client = null;
		this.player = null;
		this.map = null;
		this.fog = null;
		this.fog2 = null;
		this.survivors = [];
		this.survivorGroup = null;
		//this.text = null;
	}

	Game.prototype = {

		create: function () {
			this.game.world.setBounds(0, 0, 64000, 64000);
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			//var style = { font: "65px Arial", fill: "#ff0044", align: "center" };

			this.player = new Player(this.game, this.map);
			this.map = new Map(this.game,this.player, this);
			this.client = new Client(this);
			this.client.create();
			this.game.camera.bounds = new Phaser.Rectangle(0, 0, 64000, 64000);
			
			//this.dtext = this.game.add.text(this.player.sprite.x, this.player.sprite.y, this.game.time.fps, style);
			//this.game.physics.enable(this.player.sprite, Phaser.Physics.ARCADE);
			//this.game.input.onDown.add(this.gofull, this);
		},

		gofull: function () {
		    this.game.stage.scale.startFullScreen();
		},

		update: function () {
			if(this.player !== null){
				this.game.physics.arcade.collide(this.player.sprite, this.map.collisionLayer);
				this.game.physics.arcade.collide(this.player.sprite, this.map.fogLayer);
				this.player.update();
			}
			//this.dtext.setText(this.game.time.fps);

			//console.log(this.game.time.fps );
			if(this.client !== null)
				this.client.update();

			if(this.fog !== null){
				this.fog.update();
			}else if(this.map.fogLayer !== null){
				this.fog = new Fog(this.game, this.player.sprite, this.map.fogLayer);
				this.fog.create();
			}
			
			if(this.fog2 !== null){
				this.fog2.update();
			}else if(this.map.collisionLayer !== null){
				this.fog2 = new Fog(this.game, this.player.sprite, this.map.collisionLayer, 35);
				this.fog2.create();
			}
		},
		render: function () {

			// Physics
			//this.game.debug.body(this.player.sprite);
		}


	};

	window['phaser'] = window['phaser'] || {};
	window['phaser'].Game = Game;

}());
