'use strict';

function Survivor(id, game) {
	this.id = id;
	this.game = game;
	this.sprite = null;
};

Survivor.prototype = {

	create: function (x, y) {
		this.sprite = this.game.survivorGroup.getFirstDead();
		this.sprite.animations.add('walk', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 18, true);
		this.sprite.animations.add('run', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 30, true);
		this.sprite.animations.add('stand', [4], 15, true);
		this.sprite.anchor.setTo(0.5, 0.5);
		this.sprite.scale.x = 1.4;
		this.sprite.scale.y = 1.4;
		this.sprite.reset(x, y);
		this.game.survivors.push(this);
	},

	update: function() {
		//should also do perdiction here.
		if(this.speed > 150){
			this.sprite.animations.play('run');
		}else if(this.speed > 0){
			this.sprite.animations.play('walk');
		}else{
			this.sprite.animations.play('stand');
		}
	}

};